//
//    chalk, a chatroom server
//    Copyright (C) 2019 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

use chalk_proto::{Message,Command};
use std::sync::mpsc::{Receiver,Sender};
use std::sync::atomic::{AtomicBool,Ordering};
use std::sync::{Arc,RwLock};
use crate::buffer::MessageBuffer;
use chrono::prelude::*;
use std::time::Duration;
use std::thread;
use std::os::unix::net::UnixStream;
use std::io::{BufReader,BufRead,LineWriter,Write};

pub fn message_handler(buf: Arc<RwLock<MessageBuffer>>, rx: Receiver<Message>,log: slog::Logger) {
    loop {
        let message = rx.recv().expect("Main thread hung up");
        debug!(log, "Got message"; o!("message" => format!("{}",message)));
        buf.write().expect("Poisoned!").push(message).expect("Buffer corrupted, exiting");
        //TODO: log message
    }
}
pub fn timer(tx: Sender<Message>) {
    let mut old_day = Local::now().day();
    loop {
        let local: DateTime<Local> = Local::now();
        if local.day() != old_day {
            old_day = local.day();
            tx.send(Message::date());
            //Sleep for most of a day
            thread::sleep(Duration::from_secs(24*60*60-30))
        } else {
            //Sleep for a few seconds
            thread::sleep(Duration::from_secs(5))
        }
    }
}
pub fn handle_client(stream: UnixStream, buf: MessageBuffer, tx: Sender<Message>, chlog: slog::Logger) {

    debug!(chlog,"Spinning up client handler thread");

    let log_clone = chlog.new(o!());

    let kill = Arc::new(AtomicBool::new(false));
    let k_clone = kill.clone();
    let s_clone = stream.try_clone().expect("Couldn't clone socket");
    //Spawn recieving thread
    thread::spawn(move || handle_recieving(s_clone,tx,k_clone,log_clone));

    //Spawn sender thread
    handle_sending(stream,buf,kill,chlog);
}

pub fn handle_recieving(mut root_stream: UnixStream,tx: Sender<Message>,kill: Arc<AtomicBool>,chlog: slog::Logger) {
    let mut stream = BufReader::new(root_stream.try_clone().unwrap());
    let mut username = "anon".to_owned();
    let mut client = "raw".to_owned();
    loop {
        if kill.load(Ordering::Relaxed) {
            trace!(chlog,"Reciever hanging up"; o!("user" => username));
            return;
        }
        let mut buf = String::new();

        let result = stream.read_line(&mut buf);
        match result {
            //Disconnect the user if we get a read of 0 bytes
            Ok(0) => {
                tx.send(Message::part(username.clone()));
                //Set kill to true so the sender hangs up
                kill.store(true,Ordering::Relaxed);
                debug!(chlog,"User disconnected"; o!("user" => username));
                return;
            },
            Ok(_) => {
                let command = buf.as_str().into();
                match command {
                    Command::Client(name) => {
                        debug!(chlog,"Got client change message");
                        client = name.clone();
                    },
                    Command::Login(name) => {
                        debug!(chlog,"Got login message");
                        username = name.clone();
                        tx.send(Message::join(name.trim().to_string()));
                    },
                    Command::Message(body) => {
                        debug!(chlog,"Got message from client");
                        if client == "term" || client == "terminal" {
                            root_stream.write(b"\x1b[1A\x1b[0K\r");
                        }
                        tx.send(Message::message(username.clone(),body.trim().to_string()));
                    }
                }
            },
            //Disconnect the user if we get any error
            Err(e) => {
                debug!(chlog,"Got error, exiting message handler"; o!("error" => format!("{:?}",e)));
                tx.send(Message::part(username.clone()));
                //Set kill to true so the sender hangs up
                kill.store(true,Ordering::Relaxed);
                return;
            }
        }
    }
}
pub fn handle_sending(stream: UnixStream, mut buf: MessageBuffer, kill: Arc<AtomicBool>, chlog: slog::Logger) {
    let mut stream = LineWriter::new(stream);

    //Start by sending backscroll
    let messages = match buf.all_messages(){
        Ok(vec) => vec,
        Err(_) => {
            kill.store(true,Ordering::Relaxed);
            error!(chlog,"Failed to read message buffer");
            return;
        }
    };
    debug!(chlog,"Sending backscroll"; o!("# of messages" => messages.len()));
    for message in messages {
        //Write a message and exit if we can't
        if let Err(_) = stream.write_fmt(format_args!("{}\n",message)) {
            kill.store(true,Ordering::Relaxed);
            debug!(chlog,"Failed to write to user");
            return;
        }
    }

	// This lets us fulfill the AGPL
    stream.write_all(b"Welcome to chalk! Source at https://gitlab.com/waylon531/chalk\n");
    
    //Then move on to sending messages as they arrive
    loop {
        let messages = match buf.new_messages(){
            Ok(vec) => vec,
            Err(_) => {
                kill.store(true,Ordering::Relaxed);
                error!(chlog,"Failed to read message buffer");
                return;
            }
        };
        for message in messages {
            //Write a message and exit if we can't
            if let Err(_) = stream.write_fmt(format_args!("{}\n",message)) {
                kill.store(true,Ordering::Relaxed);
                debug!(chlog,"Failed to write to user");
                return;
            }
        }

        //We sleep to be more efficient
        thread::sleep(Duration::from_millis(10));
    }
}
