//
//    chalk, a chatroom server
//    Copyright (C) 2019 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//


#[macro_use]
extern crate slog;
extern crate slog_term;
extern crate slog_async;

#[cfg(test)]
#[macro_use]
extern crate proptest;

use slog::Drain;

mod buffer;
mod error;
mod handlers;
#[cfg(test)]
mod tests;

use std::fs;
use std::fs::Permissions;
use std::os::unix::net::UnixListener;
use std::os::unix::fs::PermissionsExt;
use std::sync::mpsc::channel;
use std::sync::{Arc,RwLock};
use std::thread;

use self::buffer::MessageBuffer;
use self::handlers::{timer,message_handler,handle_client};

use serde_derive::Deserialize;
use docopt::Docopt;

const USAGE: &'static str = "
chalk

Usage:
  chalk [options]
  chalk (--help|--version)

Options:
  -s PATH --socket PATH     Specify the path to the socket to listen on
  -h --help                 Show this screen.
  --version                 Show version.
";

#[derive(Debug, Deserialize)]
struct Args {
    flag_socket: Option<String>,
}


fn main() -> std::io::Result<()> {
    //Parse command line args
    let args: Args = Docopt::new(USAGE)
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());

    //Set up logging
    let decorator = slog_term::TermDecorator::new().build();
    let drain = slog_term::FullFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();

    let _log = slog::Logger::root(drain, o!());
    info!(_log,"Starting chalk");

    //Create socket 
    let socket = match args.flag_socket {
        Some(s) => s,
        None => "/tmp/chalk.socket".to_owned()
    };
    info!(_log, "Started listening"; o!("location" => &socket));
    //Remove old socket
    fs::remove_file(&socket)?;
    //Start listening
    let listener = UnixListener::bind(&socket).unwrap();
    //And then set the right permissions
    let permissions = Permissions::from_mode(0o777);
    fs::set_permissions(&socket,permissions)?;

    //Create our master message buffer
    let buffer = Arc::new(RwLock::new(MessageBuffer::new()));
    //And a channel for our message handler
    let (tx, rx) = channel();

    let message_log = _log.new(o!("handler" => "messages"));

    //Clone the buffer so we can move it inside this thread
    let b = buffer.clone();
    thread::spawn(move || message_handler(b,rx,message_log));


    //Create a thread that sends out daily messages about the date
    let t = tx.clone();
    thread::spawn(move || timer(t));

    info!(_log, "Ready to handle clients");

    // accept connections and process them, spawning a new thread for each one
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                let chlog = _log.new(o!());
                //Clone these so we can move them inside the new thread
                let b = buffer.read().expect("Poisoned!").clone();
                let t = tx.clone();
                /* connection succeeded */
                thread::spawn(|| handle_client(stream,b,t,chlog));
            }
            Err(_err) => {
                /* connection failed */
                break;
            }
        }
    }
    Ok(())
}
