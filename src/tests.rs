//
//    chalk, a chatroom server
//    Copyright (C) 2019 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//


mod ringbuffer {
    use crate::buffer::{RingBuffer,BUFFERSIZE};
    use proptest::prelude::*;
    proptest! {
        #[test]
        fn push_pop(
                mut stuff in prop::collection::vec(0..9999i32, 1..100),
            ) {
            //Create buffer
            let mut buf = RingBuffer::new();
            for element in stuff.iter() {
                buf.push(*element);
            }

            //Check last element
            assert!(*buf.pop() == stuff.pop());
        }
        //This test checks to make sure our buffer has the right
        //number of elements
        #[test]
        fn buffer_size(
                stuff in prop::collection::vec(0..9999i32, 1..100),
            ) {
            //Create buffer
            let mut buf = RingBuffer::new();
            for element in stuff.iter() {
                buf.push(*element);
            }
            let pos = buf.get_position();

            //Create a slice of the elements of the buffer
            let slice = buf.slice_from((pos+1) % BUFFERSIZE);
            //It's capped out
            let len = if stuff.len() >= BUFFERSIZE-1 {
                BUFFERSIZE - 1
            } else {
            //Or it's not and it only has a few elements
                stuff.len()
            };
            assert_eq!(slice.into_iter().filter_map(|x| x).collect::<Vec<i32>>().len(),len);
        }
        #[test]
        fn push_length(
                num in 0..1000usize
            ) {
            //Create buffer
            let mut buf = RingBuffer::new();
            for _ in 0..num {
                buf.push(0usize);
            }

            //Check to see if we're at the write position
            //
            //After reaching BUFFERSIZE we should wrap back around
            assert!(buf.get_position() == num % BUFFERSIZE);
        }
    }

}

mod messagebuffer {
    use crate::buffer::{MessageBuffer,BUFFERSIZE};
    use chalk_proto::Message;
    use proptest::prelude::*;
    proptest! {
        #[test]
        fn push_length(
                num in 0..1000usize
            ) {
            //Create buffer
            let mut buf = MessageBuffer::new();
            for _ in 0..num {
                buf.push(Message::date());
            }

            //Check to see if we're at the write position
            //
            //After reaching BUFFERSIZE we should wrap back around
            assert!(buf.get_position() == num % BUFFERSIZE);
        }
    }

}
