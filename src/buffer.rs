//
//    chalk, a chatroom server
//    Copyright (C) 2019 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//


pub const BUFFERSIZE: usize = 100;

use chalk_proto::Message;
use crate::error::Error;

use arr_macro::arr;

use std::ops::Index;
use std::sync::{Arc,RwLock};

pub struct RingBuffer<T: Sized+Clone> {
    buffer: [Option<T>; BUFFERSIZE],
    position: usize,
}

impl<T: Sized + Clone> RingBuffer<T> {
    pub fn new() -> RingBuffer<T> {
        RingBuffer {
            //TODO: find out how to reuse buffersize here
            buffer: arr![None; 100],
            position: 0
        }
    }
    pub fn push(&mut self, data: T) {
        self.buffer[self.position] = Some(data);
        self.position += 1;
        // Wrap position back around if we go outside the buffer
        if self.position >= BUFFERSIZE {
            self.position = 0;
        }
    }
    #[allow(dead_code)]
    pub fn pop(&mut self) -> &Option<T> {
        if self.position == 0 {
            //Wrap back around
            self.position = BUFFERSIZE - 1;
        } else {
            // or just go down 1 position
            self.position -= 1;
        }
        &self.buffer[self.position]
    }
    pub fn slice_from(&self, index: usize) -> Vec<Option<T>> {
        if index <= self.position {
            self.buffer[index .. self.position].to_vec()
        } else {
            let mut vec =self.buffer[index .. BUFFERSIZE].to_vec();
            vec.extend(self.buffer[0..self.position].iter().cloned());
            vec
        }
    }
    pub fn get_position(&self) -> usize {
        self.position
    }

}
impl<T: Sized + Clone> Index<usize> for RingBuffer<T> {
    type Output = Option<T>;
    fn index(&self, id: usize) -> &Self::Output {
        &self.buffer[id % BUFFERSIZE]
    }
}

#[derive(Clone)]
pub struct MessageBuffer {
    messages: Arc<RwLock<RingBuffer<Message>>>,
    position: usize
}

impl MessageBuffer {
    pub fn new() -> MessageBuffer {
        MessageBuffer {
            messages: Arc::new(RwLock::new(RingBuffer::new())),
            position: 0
        }
    }
    pub fn new_messages(&mut self) -> Result<Vec<Message>,Error> {
        let inner = self.messages.read()?;
        let messages = inner.slice_from(self.position);
        self.position = inner.get_position();
        //Filter out all empty messages
        Ok(messages.into_iter().filter_map(|x| x).collect())
    }
    /// This method gets all messages except for the last one
    pub fn all_messages(&self) -> Result<Vec<Message>,Error> {
        let inner = self.messages.read()?;
        let position = inner.get_position();
        // We start getting messages from the index right after
        // the start of our ring buffer
        //
        // This gives us BUFFERSIZE-1 messages
        let next_position = (position + 1) % BUFFERSIZE;
        let messages = inner.slice_from(next_position);
        //Filter out all empty messages
        Ok(messages.into_iter().filter_map(|x| x).collect())

    }
    pub fn push(&mut self, data: Message) -> Result<(),Error> {
        self.position+=1;
        if self.position >= BUFFERSIZE {
            self.position = 0;
        }
        self.messages.write()?.push(data);
        Ok(())
    }
    pub fn get_position(&self) -> usize {
        self.position
    }
}
