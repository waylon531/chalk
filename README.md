# chalk

`chalk` is a chatroom server designed for multiple users on the same
system to talk to each other. This is accomplished through a unix socket
with a default location at /tmp/chalk.socket, but the location can be
overridden with the `-s PATH` flag.

The protocol is pretty simple, all things sent to the socket are treated
as messages except for commands that are prefixed with a slash. The only
command that is implemented is currently `/login <username>`, which sets
your username for future messages. See https://gitlab.com/waylon531/chalk-proto
